---
title: "Pasta with Mushrooms"
date: "2021-10-04"
description: "Quick pasta with mushrooms in cheese sauce"
tags: ["vegetarian", "onions", "pasta", "mushroom", "soft-cheese", "stock"]
---
## Ingredients

For 2 people:

* 1 green onions
* 1/2 red onion
* 6-8 mushrooms
* 250 g spaghetti
* 100 ml stock
* 100 g soft cheese with herbs
* 1 table spoon of oil

## Method of preparation

Heat up some water for the pasta. In the meantime cut the mushrooms and onions.
Add the pasta to the water. Stir-fry the red onions and mushroom in a separate
pan. Add the green onions just before the mushrooms are ready. Then add the
vegetable stock and stir in the soft-cheese. Add some herbs (preferably thyme),
pepper and some salt.

Serve the pasta with the sauce. Enjoy!
