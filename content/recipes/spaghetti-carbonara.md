---
title: "Spaghetti Carbonara"
date: "2021-10-19"
description: ""
tags: ["spaghetti", "carbonara", "bacon", "cream"]
---
## Ingredients

For two portions:

* 1 table spoon of olive oil or some butter
* 1/2 onion
* 1/2 clove of garlic
* 100g of bacon cubes
* Some Parmesan
* Some garden parsley
* 150ml Cream
* 1 egg yolk
* 250g Spaghetti

## Method of preparation

Put the spaghettis in boiling water and cook them al dente. In the
meantime finely chop the garlic and the onions. Then fry the bacon. A
little later you can add the garlic and onions.

Add the cream to the pan as soon as the bacon is fried. Add the garden
parsley, as well as some salt and plenty of pepper. Afterwards stier in
the Parmesan.

You can teem the spaghetti and add the egg yolk. Briefly mix it into the
spaghetti and and add the carbonara sauce.

Enjoy!

