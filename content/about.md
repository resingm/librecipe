---
title: "About"
disableShare: true
searchHidden: true
---
This is a website aiming to deliver recipes in a minimal way. That is, no overused JavaScript, no tracking, no advertisements, no life stories. Just recipes. Period. The website name is a wordplay of libre and recipes.

This website thrives on community contributions, so please consider [contributing](/contributing).
